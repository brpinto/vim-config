execute pathogen#infect()
filetype plugin indent on

set nocompatible
syntax on
filetype plugin indent on

"init Pathogen
exec pathogen#infect()

"formatting
set enc=utf-8
set nowrap "don't wrap lines
set ts=2 sts=4 sw=4 expandtab "a tab is 4 spaces
set list "show invisible chars
set lcs=tab:▸\ ,eol:¬,trail:· "set invisible chars marks

"searching
set hlsearch "highlight matches
set incsearch "incremental search
set ignorecase "case insesitive search
set smartcase "smart case insensitive 

"searching commands
set wildmenu "autocomplete commands
set wildmode=full "full command autocomplete
set showcmd "display incomplete commands

"set backup files location
set backupdir=~/.vim/backup,/tmp,.
set directory=~/.vim/backup,/tmp,.

set autoread "auto load file changes
set hidden "don't warn me of modified buffers

set diffopt+=vertical "split panels vertically

"load all other settings files
let configs = [
            \'keymappings.vim',
            \'theme.vim',
            \'functions.vim'
            \]

for file in configs
    exec 'source ~/.vim/'.file
endfor
