nmap <right> :cnext<CR>
nmap <left> :cprevious<CR>

imap jj <Esc>

"windows control mapping
nnoremap § <C-W>

"CtrlP config
set wildignore+=*/tmp/*,*.so,*.swp,*.zip,*.gz "ignore files
nmap <Tab> :CtrlPBuffer<CR>
nmap <C-o> :CtrlPBufTag<CR>
nmap <Leader>f :CtrlPLine<CR>
let g:ctrlp_working_path_mode = 'ra'
let g:ctrlp_custom_ignore = {
            \ 'dir': '\v[\/](target|dist|node_modules|bower_components|coverage|reports)$'
            \}

"NERDTree config
nmap <C-n> :NERDTreeToggle<CR>
nmap <Leader>p :NERDTreeFind<CR>
let NERDTreeMinimalUI = 1 "hide line numbers and help menu
let NERDTreeQuitOnOpen = 1 "close nerdtree when opening buffer
let NERDTreeDirArrows = 1 "use pretty arrows 
"let NERDTreeChDirMode = 2 "change CWD to NERDTree root
let NERDTreeShowBookmarks = 1 "show bookmarkets by default

"TagBar config
let g:tagbar_ctags_bin = '/usr/local/bin/ctags' "use exuberant tags

"Gundo config
nmap <Leader>u :GundoToggle<CR>
let g:gundo_width = 30 "default is 45
let g:gundo_right = 1
let g:gundo_preview_bottom = 1

"GitV
let g:Gitv_WrapLines = 1
let g:Gitv_TruncateCommitSubjects = 1
let g:Gitv_OpenHorizontal = 1

" Tern keys
let g:tern_map_keys = 1
" The above option enables the following shortcuts:
" <Leader>td :TernDoc - display documentation
" <Leader>tR :TernRename - rename symbol under cursor
" <Leader>tr :TernRefs - show where symbol is being used
" <Leader>tt :TernType - show data type of params and return type
" Opens a panel with the definition of the symbol under cursor
nmap <Leader>tp :TernDefPreview<CR>

" Emmet
let g:user_emmet_expandabbr_key = '<C-e>'

" Pretty Format JSON
nmap =j :%!python -m json.tool<CR>

"CtrlSF
let g:ctrlsf_default_root = 'cwd'
let g:ctrlsf_position = 'bottom'
nmap <C-F> :CtrlSF
