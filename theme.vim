set number "show line numbers
set ruler "show line and column number
set laststatus=2 "show status line at all times
set cpoptions+=$ "add selection delimiter

set guifont=Fira\ Mono:h14
syntax on

if has('gui_running')
    set bg=dark
    colorscheme base16-ocean
endif
