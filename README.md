# How to Install

Clone this repository to `$HOME/.vim/`. Create a _symlink_ to `vimrc` file so
it sits on `$HOME/.vimrc` as well. These are the default locations Vim is
expecting to find its configurations and plugins installed.

Each plugin comes from another repository and is included in this one as a
submodule. Also, some plugins may need extra work to be installed.

You can clone the repository in several different ways:

+ clone the repository and submodules: `git clone --recursive <url>`
+ clone repository and then init submodules:
    + `git clone <url>`
    + `git submodule update --init --recursive`

Some plugins may depend on others or on some external tool. For instance:

- **tern**: depends on node's _tern_ module, so after fetching the plugin's
  repository, `cd` into the plugin's folder and run `npm install`;
- **syntastic**: expects to find some lint executable on the `$PATH`, like
  _jshint_. Just run `sudo npm install -g jshint` i.e. to have _jshint_
available globally. Syntastic will immediately use it for _javascript_ files;
- **extradite**: requires _fugitive_;
- **airline**: expects _fugitive_ to also be present to display what's the
  current working git branch;
- **YouCompleteMe**: needs `cmake` to be in the system to run `install.sh`;
