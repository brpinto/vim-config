"auto append parenthesis
inoremap ( ()<Left>
inoremap <expr> )  strpart(getline('.'), col('.')-1, 1) == ")" ? "\<Right>" : ")" 

"autoclose brackets
autocmd FileType c,c++,php,java,javascript,typescript,css inoremap {<CR> {<CR>}<Esc>=%o
